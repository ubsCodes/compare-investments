import * as types from '../constants/types'

const QUERY = 'AAPL,FB,GOOG'; 
const API = `https://financialmodelingprep.com/api/company/real-time-price/${QUERY}?datatype=json`;

export const getProducts = () =>  
  dispatch =>
    //fetch(`products.json`)   
    fetch(API)
      .then(response => console.log(response.json()) )
      /*.then(response => {
        dispatch({
          type: types.FETCH_PRODUCTS,
          payload: response.products
        })
      })*/

export const compare = product => ({
    type: types.COMPARE_PRODUCT,
    product
  })
